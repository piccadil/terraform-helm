# A module for deploying AKS and demo application

This module contains three submodules:
* Module `aks-vnet-ingress` that deploys AKS, vnet, nginx as an ingress controller
* Module that deploys application with Kubernetes provider and uses Deployment, Service and Ingress resources
* Module that deploys application with Helm provides and uses local [chart](.helm/charts/demo-app) and uses [values.yaml](.helm/values/values_dev.yaml) file

### Running terraform

Before you start, please create azure storage account and container to keep terraform state.
Also configure Service Principal with Secret. After this, to configure with terraform, please provide following environment variables

```
export ARM_CLIENT_ID="..."
export ARM_CLIENT_SECRET="..."
export ARM_SUBSCRIPTION_ID="..."
export ARM_TENANT_ID="..."
export TF_VAR_client_id=$ARM_CLIENT_ID
export TF_VAR_client_secret=$ARM_CLIENT_SECRET
```
After providing Service Principal data, please run `terraform init` with backend config, e.g.:
```
terraform init \
-backend-config="resource_group_name=tfstate" \
-backend-config="storage_account_name=tfstateterraformhelm" \
-backend-config="container_name=tfstate" \
-backend-config="key=aks-app.tfstate"
```

Sample `terraform plan` command output can be found [here](plan.txt)