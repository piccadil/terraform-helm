module "dev-cluster" {  
    aks_cluster_name = "tf-dev-cluster"
    source = "./modules/aks-vnet-ingress"
    resource_group_name = "tf-dev-rg-1"
    client_id = var.client_id
    client_secret = var.client_secret
    location = "Germany West Central"
    vnet_name = "tf-dev-vnet"
    vnet_address_space = ["10.0.0.0/16"]
    vnet_subnets  = [
        {
        name           = "subnet01"
        address_prefix = "10.0.1.0/24"
        },
        {
        name           = "subnet02"
        address_prefix = "10.0.2.0/24"
        }
    ]
    tags = {    
        Terraform   = "true"  
        Environment = "dev"   
    }
}

module "demo-app" {  
    source = "./modules/deploy-app"
    deploy_name = "demo-app"
    deploy_namespace = "demo-app"
    kube_config_raw = module.dev-cluster.kube_config_raw
    deploy_labels = {
        app = "demo-app"
        env = "dev"
    }
    deploy_replica_count = 1
    deploy_image = "mcr.microsoft.com/azuredocs/aks-helloworld:v1"
    deploy_limits = {
        cpu    = "0.5"
        memory = "512Mi"
    }
    deploy_requests = {
        cpu    = "250m"
        memory = "50Mi"
    }
    deploy_service_type = "ClusterIP"
    deploy_service_target_port = 80
    deploy_service_port = 80
    deploy_ingress_annotations = {
        "kubernetes.io/ingress.class" = "nginx"
        "nginx.ingress.kubernetes.io/ssl-redirect" = "false"
    }
    deploy_ingress_paths = [{
    service_name = "demo-app-service"
    service_port = "80"
    path = "/"
  }] 
}


module "helm-demo-app" {  
    source = "./modules/helm-deploy-app"
    kube_config_raw = module.dev-cluster.kube_config_raw
    helm_deploy_release_name = "helm-demo-app"
    helm_deploy_repository = ".helm/charts/"
    helm_deploy_chart = "demo-app"
    helm_deploy_namespace = "helm-demo-app"
    helm_deploy_create_namespace = true
    helm_deploy_values_file = ["${file(".helm/values/values_dev.yaml")}"]
}
            