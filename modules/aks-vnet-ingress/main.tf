resource "azurerm_resource_group" "rg" {
  name                = var.resource_group_name
  location            = var.location
  tags = var.tags
}

resource "azurerm_kubernetes_cluster" "aks" {
    name                = var.aks_cluster_name
    location            = azurerm_resource_group.rg.location
    resource_group_name = azurerm_resource_group.rg.name
    dns_prefix          = var.aks_dns_prefix

    default_node_pool {
        name            = var.aks_node_pool_name
        node_count      = var.aks_node_pool_count
        vm_size         = var.aks_node_pool_vm_size
    }

    service_principal {
        client_id     = var.client_id
        client_secret = var.client_secret
    }

    network_profile {
        load_balancer_sku = var.aks_vnet_lb_sku
        network_plugin = var.aks_vnet_network_plugin
    }
    role_based_access_control {
      enabled = true
    }
    tags = var.tags
}


resource "azurerm_virtual_network" "vnet" {
  name                =  var.vnet_name
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = var.vnet_address_space
  dynamic "subnet" {
    for_each = var.vnet_subnets
 
    content {
      name           = subnet.value.name
      address_prefix = subnet.value.address_prefix
    }
  }
  tags = var.tags
 }

resource "local_file" "kube_config" {
    content  = azurerm_kubernetes_cluster.aks.kube_config_raw
    filename = "config"
}

provider "helm" {
  kubernetes {
    config_path = local_file.kube_config.filename
  }
}


resource "helm_release" "ingress-nginx" {
  name       = var.helm_ingress_name
  repository = var.helm_ingress_repository
  chart      = "ingress-nginx"
  namespace  = var.helm_ingress_namespace
  create_namespace  = var.helm_ingress_create_namespace
}