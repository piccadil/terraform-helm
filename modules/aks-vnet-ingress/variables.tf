variable resource_group_name {
    description = "Name of resource group."  
    type        = string
}
variable location {
    description = "Name of location."  
    type        = string
}
variable aks_cluster_name {
    description = "Name of the aks cluster. Must be unique."  
    type        = string
}
variable "client_id" {
    description = "Client id of service principal."  
    type        = string
    sensitive = true
}
variable "client_secret" {
    description = "Secret of service principal."  
    type        = string
    sensitive = true
}

variable aks_node_pool_name {
    description = "Name of default node pool."  
    type        = string
    default     = "default"
}
variable aks_node_pool_count {
    description = "Number of hosts in pool"  
    type        = number
    default     = 1
}    
variable aks_node_pool_vm_size {
    description = "Node pool vm size"  
    type        = string
    default     = "Standard_D2_v2"
}

variable aks_dns_prefix {
    description = "DNS prefix for kubernetes"  
    type        = string
    default     = "kube"
}

variable aks_vnet_network_plugin {
    description = "Vnet plugin name. Could be azure or kubenet"  
    default     =  "kubenet"
    type        =  string
}
variable aks_vnet_lb_sku {
    default = "standard"
    description = "Load balancer SKU"
    type        =  string
}
variable tags {  
    description = "Tags to set on the AKS."
    type        = map(string)  
    default     = {}
}

variable vnet_name {
    description = "Name of virtual network. Mus be unique."  
    type        = string
    default     = "tf_vnet"
}
variable vnet_address_space {
    description = "Address space of vnet"  
    type        = list  
    default     = ["10.0.0.0/16"]
}

variable vnet_subnets {
    type = list
    description = "A list of subnets"
    default = [
    {
      name           = "subnet01"
      address_prefix = "10.0.1.0/24"
    },
 
    {
      name           = "subnet02"
      address_prefix = "10.0.2.0/24"
    }
  ]
  
}

variable helm_ingress_repository {
    default = "https://kubernetes.github.io/ingress-nginx"
    description = "URL of ingress repository."
    type = string
}

variable helm_ingress_name {
    type = string
    description = "Name of ingress release. Defaults to ingress-nginx"
    default = "ingress-nginx"
}
variable helm_ingress_namespace {
    type = string
    description = "Namespace to install ingress release. Defaults to ingress-nginx"
    default = "ingress-nginx"
}
variable helm_ingress_create_namespace {
    type = bool
    description = "Create the namespace if it does not yet exist. Defaults to true"
    default = true
}
