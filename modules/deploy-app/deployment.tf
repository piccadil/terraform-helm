resource "kubernetes_deployment" "deployment" {
  metadata {
    name = var.deploy_name
    labels = var.deploy_labels
    namespace = var.deploy_namespace
  }

  spec {
    replicas = var.deploy_replica_count

    selector {
      match_labels = var.deploy_labels
    }

    template {
      metadata {
        labels = var.deploy_labels
      }

      spec {
        container {
          image = var.deploy_image
          name  = "${var.deploy_name}-container"

          resources {
            limits = var.deploy_limits
            requests = var.deploy_requests
          }

        }
      }
    }
  }
}
