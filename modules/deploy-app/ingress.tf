resource "kubernetes_ingress" "ingress" {
  metadata {
    name = "${var.deploy_name}-ingress"
    namespace = var.deploy_namespace
    annotations = var.deploy_ingress_annotations
  }

  spec {
    backend {
      service_name = "${var.deploy_name}-service"
      service_port = var.deploy_service_port
    }

    rule {
      http {
        dynamic "path" {
          for_each = var.deploy_ingress_paths
          content{
            backend {
            service_name = path.value["service_name"]
            service_port = path.value["service_port"]
          }
          path = path.value["path"]
        }
        }

      }
    }
  }
}
