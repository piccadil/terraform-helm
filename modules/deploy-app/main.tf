resource "local_file" "kube_config" {
    content  = var.kube_config_raw
    filename = "kube_config"
}

provider "kubernetes" {
  config_path    =  local_file.kube_config.filename
}

resource "kubernetes_namespace" "namespace" {
  metadata {
    name = var.deploy_namespace
  }
}

