resource "kubernetes_service" "service" {
  metadata {
    name = "${var.deploy_name}-service"
    namespace = var.deploy_namespace
  }
  spec {
    selector = var.deploy_labels
    port {
      port        = var.deploy_service_port
      target_port = var.deploy_service_target_port
    }

    type = var.deploy_service_type
  }
}