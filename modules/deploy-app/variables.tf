variable deploy_name {
    type = string
    description = "Name of ingress release."
}
variable deploy_namespace {
    type = string
    description = "Namespace to deploy release."
}

variable kube_config_raw {
    type = string
    description = "Kube config raw"
}

variable deploy_labels {
    type        = map(string)  
    default     = {} 
}
variable deploy_ingress_annotations {
    type        = map(string)  
    default     = {
        "kubernetes.io/ingress.class" = "nginx"
    } 
}
variable deploy_replica_count {
    type = number
    description = "Number of deployment replicas"
    default = 1
}

variable deploy_image {
    type = string
    description = "Image to use in deployment"
}

variable deploy_limits {
    type        = map(string)  
    default     = {} 
}

variable deploy_requests {
    type        = map(string)  
    default     = {} 
}

variable deploy_service_type {
    type = string
    description = "Type of service - ClusterIP, LoadBalancer or NodePort. Defaults to ClusterIP"
    default = "ClusterIP"
}
variable deploy_service_target_port {
    type = number
    description = "Service target port"
}
variable deploy_service_port {
    type = number
    description = "Service port"
}

variable deploy_ingress_path {
    type = string
    description = "Ingress http path"
    default = "/"
}

variable deploy_ingress_paths {
    type = list
    description = "A list of ingress paths"
}

