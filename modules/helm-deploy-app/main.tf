resource "local_file" "kube_config" {
    content  = var.kube_config_raw
    filename = "helm_kube_config"
}

provider "helm" {
  kubernetes {
    config_path = local_file.kube_config.filename
  }
}


resource "helm_release" "ingress-nginx" {
  name       = var.helm_deploy_release_name
  repository = var.helm_deploy_repository
  chart      =  var.helm_deploy_chart
  namespace  = var.helm_deploy_namespace
  create_namespace  = var.helm_deploy_create_namespace
  values = var.helm_deploy_values_file
}