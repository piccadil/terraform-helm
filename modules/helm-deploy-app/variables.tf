variable kube_config_raw {
    type = string
    description = "Kube config raw"
}

variable helm_deploy_repository {
    default = "https://kubernetes.github.io/ingress-nginx"
    description = "Path to repository. Can be local"
    type = string
}

variable helm_deploy_release_name {
    type = string
    description = "Name of release."
}
variable helm_deploy_namespace {
    type = string
    description = "Namespace to install release."
}
variable helm_deploy_create_namespace {
    type = bool
    description = "Create the namespace if it does not yet exist. Defaults to true"
    default = true
}
variable helm_deploy_chart {
    type = string
    description = "Helm chart name"
}

variable helm_deploy_values_file {
    type = list
    description = "Values files to template release"
}