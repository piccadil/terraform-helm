output "kube_config_raw" {
    value = module.dev-cluster.kube_config_raw
    sensitive = true
}
output "load_balancer_ip" {
    value = module.demo-app.load_balancer_ip
}